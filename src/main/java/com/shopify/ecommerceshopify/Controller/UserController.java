package com.shopify.ecommerceshopify.Controller;

import com.shopify.ecommerceshopify.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/home")
    public String homePage(){
        return "home";
    }
    @GetMapping("/login")
    public String loginPage(){
        return "login";
    }
}
