package com.shopify.ecommerceshopify.Service;

import com.shopify.ecommerceshopify.Entity.User;
import com.shopify.ecommerceshopify.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

//   Get all users (Buyers and Sellers)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
//    Add a user
    public User addUser(User user){
        user.setUsername(user.getUsername());
        user.setPassword(user.getPassword());

        return userRepository.save(user);
    }
//    Update a user
    public User updateUser(User user, int id){
        User existingUser = userRepository.findById(id).orElse(null);
        existingUser.setUsername(user.getUsername());
        existingUser.setPassword(user.getPassword());

        return userRepository.save(existingUser);
    }
//    Delete a user
    public String deleteUser(int id){
        userRepository.deleteById(id);
        return "User removed with ID of "+ id;
    }
}
