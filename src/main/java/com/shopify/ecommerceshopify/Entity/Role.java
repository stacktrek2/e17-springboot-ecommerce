package com.shopify.ecommerceshopify.Entity;

public enum Role {
    BUYER,
    SELLER,
    ADMIN
}
