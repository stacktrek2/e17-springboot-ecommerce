package com.shopify.ecommerceshopify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EcommerceshopifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceshopifyApplication.class, args);
	}

}

